package be.appwise.keyless.models.Car;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by 11501253 on 21/08/2017.
 */

public class LocationState extends RealmObject{
    @SerializedName("shift_state")
    @Expose
    private String shiftState;
    @SerializedName("speed")
    @Expose
    private Double speed;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("heading")
    @Expose
    private Integer heading;
    @SerializedName("gps_as_of")
    @Expose
    private Integer gpsAsOf;

    public String getShiftState() {
        return shiftState;
    }

    public void setShiftState(String shiftState) {
        this.shiftState = shiftState;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getHeading() {
        return heading;
    }

    public void setHeading(Integer heading) {
        this.heading = heading;
    }

    public Integer getGpsAsOf() {
        return gpsAsOf;
    }

    public void setGpsAsOf(Integer gpsAsOf) {
        this.gpsAsOf = gpsAsOf;
    }
}
