package be.appwise.keyless.models.Car;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by 11501253 on 17/08/2017.
 */

public class BatteryState extends RealmObject{
    @SerializedName("charging_state")
    @Expose
    private String chargingState;
    @SerializedName("charge_to_max_range")
    @Expose
    private Boolean chargeToMaxRange;
    @SerializedName("max_range_charge_counter")
    @Expose
    private Integer maxRangeChargeCounter;
    @SerializedName("fast_charger_present")
    @Expose
    private Boolean fastChargerPresent;
    @SerializedName("battery_range")
    @Expose
    private Double batteryRange;
    @SerializedName("est_battery_range")
    @Expose
    private Double estBatteryRange;
    @SerializedName("ideal_battery_range")
    @Expose
    private Double idealBatteryRange;
    @SerializedName("battery_level")
    @Expose
    private Integer batteryLevel;
    @SerializedName("battery_current")
    @Expose
    private Double batteryCurrent;
    @SerializedName("charge_starting_range")
    @Expose
    private Double chargeStartingRange;
    @SerializedName("charge_starting_soc")
    @Expose
    private Double chargeStartingSoc;
    @SerializedName("charger_voltage")
    @Expose
    private Integer chargerVoltage;
    @SerializedName("charger_pilot_current")
    @Expose
    private Integer chargerPilotCurrent;
    @SerializedName("charger_actual_current")
    @Expose
    private Integer chargerActualCurrent;
    @SerializedName("charger_power")
    @Expose
    private Integer chargerPower;
    @SerializedName("time_to_full_charge")
    @Expose
    private Double timeToFullCharge;
    @SerializedName("charge_rate")
    @Expose
    private Double chargeRate;
    @SerializedName("charge_port_door_open")
    @Expose
    private Boolean chargePortDoorOpen;

    public String getChargingState() {
        return chargingState;
    }

    public void setChargingState(String chargingState) {
        this.chargingState = chargingState;
    }

    public Boolean getChargeToMaxRange() {
        return chargeToMaxRange;
    }

    public void setChargeToMaxRange(Boolean chargeToMaxRange) {
        this.chargeToMaxRange = chargeToMaxRange;
    }

    public Integer getMaxRangeChargeCounter() {
        return maxRangeChargeCounter;
    }

    public void setMaxRangeChargeCounter(Integer maxRangeChargeCounter) {
        this.maxRangeChargeCounter = maxRangeChargeCounter;
    }

    public Boolean getFastChargerPresent() {
        return fastChargerPresent;
    }

    public void setFastChargerPresent(Boolean fastChargerPresent) {
        this.fastChargerPresent = fastChargerPresent;
    }

    public Double getBatteryRange() {
        return batteryRange;
    }

    public void setBatteryRange(Double batteryRange) {
        this.batteryRange = batteryRange;
    }

    public Double getEstBatteryRange() {
        return estBatteryRange;
    }

    public void setEstBatteryRange(Double estBatteryRange) {
        this.estBatteryRange = estBatteryRange;
    }

    public Double getIdealBatteryRange() {
        return idealBatteryRange;
    }

    public void setIdealBatteryRange(Double idealBatteryRange) {
        this.idealBatteryRange = idealBatteryRange;
    }

    public Integer getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(Integer batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public Double getBatteryCurrent() {
        return batteryCurrent;
    }

    public void setBatteryCurrent(Double batteryCurrent) {
        this.batteryCurrent = batteryCurrent;
    }

    public Double getChargeStartingRange() {
        return chargeStartingRange;
    }

    public void setChargeStartingRange(Double chargeStartingRange) {
        this.chargeStartingRange = chargeStartingRange;
    }

    public Double getChargeStartingSoc() {
        return chargeStartingSoc;
    }

    public void setChargeStartingSoc(Double chargeStartingSoc) {
        this.chargeStartingSoc = chargeStartingSoc;
    }

    public Integer getChargerVoltage() {
        return chargerVoltage;
    }

    public void setChargerVoltage(Integer chargerVoltage) {
        this.chargerVoltage = chargerVoltage;
    }

    public Integer getChargerPilotCurrent() {
        return chargerPilotCurrent;
    }

    public void setChargerPilotCurrent(Integer chargerPilotCurrent) {
        this.chargerPilotCurrent = chargerPilotCurrent;
    }

    public Integer getChargerActualCurrent() {
        return chargerActualCurrent;
    }

    public void setChargerActualCurrent(Integer chargerActualCurrent) {
        this.chargerActualCurrent = chargerActualCurrent;
    }

    public Integer getChargerPower() {
        return chargerPower;
    }

    public void setChargerPower(Integer chargerPower) {
        this.chargerPower = chargerPower;
    }

    public Double getTimeToFullCharge() {
        return timeToFullCharge;
    }

    public void setTimeToFullCharge(Double timeToFullCharge) {
        this.timeToFullCharge = timeToFullCharge;
    }

    public Double getChargeRate() {
        return chargeRate;
    }

    public void setChargeRate(Double chargeRate) {
        this.chargeRate = chargeRate;
    }

    public Boolean getChargePortDoorOpen() {
        return chargePortDoorOpen;
    }

    public void setChargePortDoorOpen(Boolean chargePortDoorOpen) {
        this.chargePortDoorOpen = chargePortDoorOpen;
    }
}
