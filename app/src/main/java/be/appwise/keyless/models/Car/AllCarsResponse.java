package be.appwise.keyless.models.Car;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by 11501253 on 17/08/2017.
 */

public class AllCarsResponse {

    @SerializedName("response")
    @Expose
    private List<TeslaCar> response = null;
    @SerializedName("count")
    @Expose
    private Integer count;

    public List<TeslaCar> getCarList() {
        return response;
    }

    public void setCarList(List<TeslaCar> response) {
        this.response = response;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
