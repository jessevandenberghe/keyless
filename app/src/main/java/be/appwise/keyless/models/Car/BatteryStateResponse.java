package be.appwise.keyless.models.Car;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 11501253 on 17/08/2017.
 */

public class BatteryStateResponse {

    @SerializedName("response")
    @Expose
    private BatteryState response;

    public BatteryState getBatteryState() {
        return response;
    }

    public void setBatteryState(BatteryState response) {
        this.response = response;
    }
}
