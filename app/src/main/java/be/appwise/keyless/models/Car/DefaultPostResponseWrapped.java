package be.appwise.keyless.models.Car;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 11501253 on 18/08/2017.
 */

public class DefaultPostResponseWrapped {

    @SerializedName("response")
    @Expose
    private DefaultPostResponse response;

    public DefaultPostResponse getResponse() {
        return response;
    }

    public void setResponse(DefaultPostResponse response) {
        this.response = response;
    }
}

