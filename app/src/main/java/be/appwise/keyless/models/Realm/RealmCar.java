package be.appwise.keyless.models.Realm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import be.appwise.keyless.models.Car.BatteryState;
import be.appwise.keyless.models.Car.CarState;
import be.appwise.keyless.models.Car.ClimateState;
import be.appwise.keyless.models.Car.LocationState;
import be.appwise.keyless.models.Car.TeslaCar;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by 11501253 on 21/08/2017.
 */

public class RealmCar extends RealmObject{
    @PrimaryKey
    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("car_info")
    @Expose
    private TeslaCar carInfo;

    @SerializedName("battery_state")
    @Expose
    private BatteryState batteryState;

    @SerializedName("climate_state")
    @Expose
    private ClimateState climateState;

    @SerializedName("location_state")
    @Expose
    private LocationState locationState;

    @SerializedName("car_state")
    @Expose
    private CarState carState;

    public CarState getCarState() {
        return carState;
    }

    public void setCarState(CarState carState) {
        this.carState = carState;
    }

    public RealmCar(){
        this.id = 0;
    }

    public RealmCar(TeslaCar carInfo){
        this.id = 0;
        this.carInfo = carInfo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TeslaCar getCarInfo() {
        return carInfo;
    }

    public void setCarInfo(TeslaCar carInfo) {
        this.carInfo = carInfo;
    }

    public BatteryState getBatteryState() {
        return batteryState;
    }

    public void setBatteryState(BatteryState batteryState) {
        this.batteryState = batteryState;
    }

    public ClimateState getClimateState() {
        return climateState;
    }

    public void setClimateState(ClimateState climateState) {
        this.climateState = climateState;
    }

    public LocationState getLocationState() {
        return locationState;
    }

    public void setLocationState(LocationState locationState) {
        this.locationState = locationState;
    }
}
