package be.appwise.keyless.models.Car;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 11501253 on 17/08/2017.
 */

public class ClimateStateResponse {


    @SerializedName("response")
    @Expose
    private ClimateState response;

    public ClimateState getClimateState() {
        return response;
    }

    public void setClimateState(ClimateState response) {
        this.response = response;
    }

}
