package be.appwise.keyless.models.Car;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by 11501253 on 17/08/2017.
 */

public class ClimateState extends RealmObject{

    @SerializedName("inside_temp")
    @Expose
    private Double insideTemp;
    @SerializedName("outside_temp")
    @Expose
    private Double outsideTemp;
    @SerializedName("driver_temp_setting")
    @Expose
    private Double driverTempSetting;
    @SerializedName("passenger_temp_setting")
    @Expose
    private Double passengerTempSetting;
    @SerializedName("is_auto_conditioning_on")
    @Expose
    private Boolean isAutoConditioningOn;
    @SerializedName("is_front_defroster_on")
    @Expose
    private Boolean isFrontDefrosterOn;
    @SerializedName("is_rear_defroster_on")
    @Expose
    private Boolean isRearDefrosterOn;
    @SerializedName("fan_status")
    @Expose
    private Integer fanStatus;

    public Double getInsideTemp() {
        return insideTemp;
    }

    public void setInsideTemp(Double insideTemp) {
        this.insideTemp = insideTemp;
    }

    public Double getOutsideTemp() {
        return outsideTemp;
    }

    public void setOutsideTemp(Double outsideTemp) {
        this.outsideTemp = outsideTemp;
    }

    public Double getDriverTempSetting() {
        return driverTempSetting;
    }

    public void setDriverTempSetting(Double driverTempSetting) {
        this.driverTempSetting = driverTempSetting;
    }

    public Double getPassengerTempSetting() {
        return passengerTempSetting;
    }

    public void setPassengerTempSetting(Double passengerTempSetting) {
        this.passengerTempSetting = passengerTempSetting;
    }

    public Boolean getIsAutoConditioningOn() {
        return isAutoConditioningOn;
    }

    public void setIsAutoConditioningOn(Boolean isAutoConditioningOn) {
        this.isAutoConditioningOn = isAutoConditioningOn;
    }

    public Object getIsFrontDefrosterOn() {
        return isFrontDefrosterOn;
    }

    public void setIsFrontDefrosterOn(Boolean isFrontDefrosterOn) {
        this.isFrontDefrosterOn = isFrontDefrosterOn;
    }

    public Boolean getIsRearDefrosterOn() {
        return isRearDefrosterOn;
    }

    public void setIsRearDefrosterOn(Boolean isRearDefrosterOn) {
        this.isRearDefrosterOn = isRearDefrosterOn;
    }

    public Integer getFanStatus() {
        return fanStatus;
    }

    public void setFanStatus(Integer fanStatus) {
        this.fanStatus = fanStatus;
    }
}
