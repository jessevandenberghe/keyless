package be.appwise.keyless.models.Car;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by jessevandenberghe on 25/08/2017.
 */

public class CarStateResponse {
    @SerializedName("response")
    @Expose
    private CarState response;

    public CarState getCarState() {
        return response;
    }

    public void setCarState(CarState response) {
        this.response = response;
    }
}
