package be.appwise.keyless.models.Car;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 11501253 on 18/08/2017.
 */

public class DefaultPostResponse {

    @SerializedName("result")
    @Expose
    private Boolean result;
    @SerializedName("reason")
    @Expose
    private String reason;

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}