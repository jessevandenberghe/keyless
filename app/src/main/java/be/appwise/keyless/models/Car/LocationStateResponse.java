package be.appwise.keyless.models.Car;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 11501253 on 21/08/2017.
 */

public class LocationStateResponse {
    @SerializedName("response")
    @Expose
    private LocationState response;

    public LocationState getLocationState() {
        return response;
    }

    public void setLocationState(LocationState response) {
        this.response = response;
    }
}
