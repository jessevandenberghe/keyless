package be.appwise.keyless;

import android.app.Application;
import android.content.Context;

import com.squareup.otto.Bus;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by jessevandenberghe on 28/08/2017.
 */

public class App extends Application {

    private static App app = null;
    private static RealmConfiguration config;
    private static Bus bus;
    private static boolean keyReplacerIntentRunning;
    private static boolean inGeoFence = false;

    public static boolean getInGeoFence(){return inGeoFence;}

    public static void setInGeoFence(boolean b){inGeoFence = b;}

    public static boolean getKeyReplacerIntentRunning(){return keyReplacerIntentRunning;}

    public static void setKeyReplacerIntentRunning(boolean b){
        keyReplacerIntentRunning = b;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        Realm.init(getApplicationContext());
    }

    public static Context getContext(){
        return app;
    }

    public static Bus getBus() {
        if (bus == null) {
            bus = new Bus();
        }
        return bus;
    }

    public static Realm getCoreRealmInstance() {
        if (config == null) {
            config = new RealmConfiguration.Builder()
                    .deleteRealmIfMigrationNeeded()
                    .build();
            Realm.setDefaultConfiguration(config);
        }

        return Realm.getDefaultInstance();
    }
}
