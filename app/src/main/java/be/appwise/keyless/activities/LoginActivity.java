package be.appwise.keyless.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import be.appwise.keyless.App;
import be.appwise.keyless.R;
import be.appwise.keyless.api.ApiManager;
import be.appwise.keyless.models.AccessToken;
import be.appwise.keyless.models.Car.AllCarsResponse;
import be.appwise.keyless.models.Realm.RealmCar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginActivity extends BaseActivity {

    private static String TAG = LoginActivity.class.getSimpleName();

    @BindView(R.id.et_login_emailInput)
    EditText et_login_emailInput;

    @BindView(R.id.et_login_PasswordInput)
    EditText et_login_PasswordInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.ib_login_loginButton)
    public void onLoginClick(View view){
        String email = et_login_emailInput.getText().toString();
        final String password = et_login_PasswordInput.getText().toString();

        //email = "steffen.brans@appwise.be";
        //password = "j1c2s3d4";

        ApiManager
                .getAccessToken(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AccessToken>() {
                    @Override
                    public void onCompleted() {
                        loginSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: Error on getting access token: " + e.getMessage());
                        loginFailed();
                    }

                    @Override
                    public void onNext(AccessToken accessToken) {
                        Log.i(TAG, "onCompleted: AccessToken Retrieved: " + accessToken.getAccessToken());
                        Hawk.init(App.getContext()).build();
                        Hawk.deleteAll();
                        Hawk.put("oAuthToken", accessToken);
                        Hawk.put("password", password);
                    }
                });
    }

    private void loginSuccess(){
        ApiManager
                .getCars()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AllCarsResponse>() {
                    @Override
                    public void onCompleted() {
                        Log.i(TAG, "onCompleted: Car retrieved");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: Couldn't retrieve cars: " + e.getMessage());
                        e.printStackTrace();
                        loginFailed();
                    }

                    @Override
                    public void onNext(final AllCarsResponse allCarsResponse) {
                        try {
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    RealmCar car = new RealmCar(allCarsResponse.getCarList().get(0));
                                    realm.copyToRealmOrUpdate(car);
                                    Intent teslaControlIntent = new Intent(LoginActivity.this, HomeActivity.class);
                                    startActivity(teslaControlIntent);
                                }
                            });
                        } catch (Exception e){
                            e.printStackTrace();
                        } finally {
                            realm.close();
                        }
                    }
                });
    }

    private void loginFailed(){
        Toast.makeText(this,"Invalid credentials",Toast.LENGTH_LONG).show();
    }
}
