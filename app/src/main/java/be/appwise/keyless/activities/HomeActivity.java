package be.appwise.keyless.activities;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rubensousa.bottomsheetbuilder.BottomSheetBuilder;
import com.github.rubensousa.bottomsheetbuilder.BottomSheetMenuDialog;
import com.github.rubensousa.bottomsheetbuilder.adapter.BottomSheetItemClickListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;

import be.appwise.keyless.App;
import be.appwise.keyless.R;
import be.appwise.keyless.api.ApiManager;
import be.appwise.keyless.models.Car.BatteryState;
import be.appwise.keyless.models.Car.BatteryStateResponse;
import be.appwise.keyless.models.Car.CarState;
import be.appwise.keyless.models.Car.CarStateResponse;
import be.appwise.keyless.models.Car.ClimateState;
import be.appwise.keyless.models.Car.ClimateStateResponse;
import be.appwise.keyless.models.Car.DefaultPostResponseWrapped;
import be.appwise.keyless.models.Car.LocationState;
import be.appwise.keyless.models.Car.LocationStateResponse;
import be.appwise.keyless.models.GeoFence;
import be.appwise.keyless.models.Realm.RealmCar;
import be.appwise.keyless.recievers.GeofenceBroadcastReciever;
import be.appwise.keyless.services.KeyReplacerIntentService;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import layout.keyReplacerNotification;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class HomeActivity extends BaseActivity{

    private static final String TAG = HomeActivity.class.getSimpleName();
    private static Long carId;
    private BottomSheetMenuDialog bottomSheetMenuDialog;
    private AlertDialog.Builder alertDialogBuilder;
    private Intent keyReplacerIntent;
    private static ReactiveLocationProvider reactiveLocationProvider;

    @BindView(R.id.tv_home_carDisplayName)
    TextView tv_home_carDisplayName;

    @BindView(R.id.pb_home_loadingIndicator)
    ProgressBar pb_home_loadingIndicator;

    @BindView(R.id.tv_home_distance)
    TextView tv_home_distance;

    @BindView(R.id.tv_home_battery)
    TextView tv_home_battery;

    @BindView(R.id.iv_home_lock)
    ImageView iv_home_lock;

    @BindView(R.id.iv_home_settings)
    ImageView iv_home_settings;

    @BindView(R.id.pb_home_lockingIndicator)
    ProgressBar pb_home_lockingIndicator;

    @BindView(R.id.sr_home_swipeContainer)
    SwipeRefreshLayout sr_home_swipeContainer;

    @BindView(R.id.sw_home_keyfob)
    Switch sw_home_keyfob;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        carId = getCar().getCarInfo().getId();

        reactiveLocationProvider = new ReactiveLocationProvider(App.getContext());

        tv_home_carDisplayName.setText(realm.where(RealmCar.class).findFirst().getCarInfo().getDisplayName());

        setupListeners();

        getCarData();
    }

    private void setupListeners() {

        sr_home_swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                sr_home_swipeContainer.setRefreshing(false);
                getCarData();
            }
        });

        alertDialogBuilder = new AlertDialog.Builder(this);

        sw_home_keyfob.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                final LocationState locationState = getCar().getLocationState();
                final ArrayList<GeoFence> geoFenceList = new ArrayList<GeoFence>();
                if(b){
                    alertDialogBuilder
                            .setTitle("Are you sure?")
                            .setMessage("This feature is in beta. Keyless can not be held responsable if your Tesla does " +
                                    "not open/close at the desired moment.")
                            .setPositiveButton("Turn On", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Toast.makeText(App.getContext(), "Key Fob enabled: Check notifications to control",Toast.LENGTH_LONG).show();
                                    GeoFence geofence = new GeoFence(carId.toString(),locationState.getLatitude(),locationState.getLongitude(),100);
                                    geoFenceList.add(geofence);
                                    addGeofence(geoFenceList);
                                    if(realm.where(RealmCar.class).findFirst().getCarState().getLocked()){
                                        keyReplacerNotification.notify(App.getContext(),"" + realm.where(RealmCar.class).findFirst().getCarInfo().getDisplayName() + ": Closed");
                                    }
                                    else{

                                        keyReplacerNotification.notify(App.getContext(),"" + realm.where(RealmCar.class).findFirst().getCarInfo().getDisplayName() + ": Opened");
                                    }
                                    //keyReplacerLocationCheckThread.start();
                                    keyReplacerIntent = new Intent(App.getContext(),KeyReplacerIntentService.class);
                                    App.setKeyReplacerIntentRunning(true);
                                    startService(keyReplacerIntent);
                                    dialogInterface.cancel();
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    sw_home_keyfob.setChecked(false);
                                    dialogInterface.cancel();
                                }
                            })
                            .show();
                }
                else{

                    Toast.makeText(App.getContext(), "Key Fob disabled", Toast.LENGTH_SHORT).show();
                    clearGeofence();
                    stopService(new Intent(App.getContext(),KeyReplacerIntentService.class));
                    App.setKeyReplacerIntentRunning(false);
                    keyReplacerNotification.cancel(App.getContext());
                    keyReplacerIntent = null;
                }
            }
        });

        setupMenuDialog();

    }

    private void addGeofence(ArrayList<GeoFence> geoFenceArrayList) {
        final GeofencingRequest geofencingRequest = createGeofencingRequest(geoFenceArrayList);
        if (geofencingRequest == null) return;

        final PendingIntent pendingIntent = createNotificationBroadcastPendingIntent();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        reactiveLocationProvider
                .addGeofences(pendingIntent, geofencingRequest)
                .subscribe(new Action1<Status>() {
                    @Override
                    public void call(Status addGeofenceResult) {
                        Log.i(TAG, "call: GeoFence Added");
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.e(TAG, "Error adding geofence.", throwable);
                    }
                });
    }

    private void clearGeofence() {
        reactiveLocationProvider.removeGeofences(createNotificationBroadcastPendingIntent()).subscribe(new Action1<Status>() {
            @Override
            public void call(Status status) {
                Log.i(TAG, "call: Geofence removed");
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                Log.e(TAG, "Error removing geofences", throwable);
            }
        });
    }

    private PendingIntent createNotificationBroadcastPendingIntent() {
        Intent intent = new Intent(this, GeofenceBroadcastReciever.class);
        return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private GeofencingRequest createGeofencingRequest(ArrayList<GeoFence> geoFenceArrayList) {
        try {
            ArrayList<Geofence> geofenceList = new ArrayList<Geofence>();
            for (GeoFence geoFence : geoFenceArrayList) {
                Geofence geofence = new Geofence.Builder()
                        .setRequestId(geoFence.getName())
                        .setCircularRegion(geoFence.getLat(), geoFence.getLng(), geoFence.getRadius())
                        .setExpirationDuration(Geofence.NEVER_EXPIRE)
                        .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                        .build();
                geofenceList.add(geofence);
            }

            return new GeofencingRequest.Builder().addGeofences(geofenceList).build();
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    private void setupMenuDialog() {
        bottomSheetMenuDialog = new BottomSheetBuilder(this, R.style.AppTheme_BottomSheetDialog)
                .setMode(BottomSheetBuilder.MODE_LIST)
                .setMenu(R.menu.options)
                .setItemClickListener(new BottomSheetItemClickListener() {
                    @Override
                    public void onBottomSheetItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.it_options_logout:
                                logout();
                                break;
                            case R.id.it_options_terms:
                                showTerms();
                                break;
                            default:
                                break;
                        }
                    }
                })
                .createDialog();
    }

    private void showTerms() {
        ;
    }

    private void logout() {
        Intent intent = new Intent(this,LoginActivity.class);
        Hawk.init(App.getContext()).build();
        Hawk.delete("oAuthToken");
        Hawk.delete("password");
        startActivity(intent);
    }

    private void getCarData(){
        pb_home_loadingIndicator.setVisibility(View.VISIBLE);
        //get battery information
        ApiManager
                .getBatteryState(carId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BatteryStateResponse>() {
                    @Override
                    public void onCompleted() {
                        Log.i(TAG, "onCompleted: Retrieved battery data");
                        processCarBatteryData();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: Failed to retrieve battery data: " + e.getMessage());
                    }

                    @Override
                    public void onNext(BatteryStateResponse batteryStateResponse) {
                        BatteryState batteryState = batteryStateResponse.getBatteryState();
                        addRealmCarAttribute(batteryState);
                    }
                });

        ApiManager.getClimateState(carId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ClimateStateResponse>() {
                    @Override
                    public void onCompleted() {
                        Log.i(TAG, "onCompleted: Retrieved climate data");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: Failed to retrieve climate data: " + e.getMessage());
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(ClimateStateResponse climateStateResponse) {
                        ClimateState climatestate = climateStateResponse.getClimateState();
                        addRealmCarAttribute(climatestate);
                    }
                });

        ApiManager.getLocationState(carId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LocationStateResponse>() {
                    @Override
                    public void onCompleted() {
                        Log.i(TAG, "onCompleted: Retrieved location data");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: Failed to retrieve location data: " + e.getMessage());
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(LocationStateResponse locationStateResponse) {
                        LocationState locationState = locationStateResponse.getLocationState();
                        addRealmCarAttribute(locationState);
                    }
                });

        ApiManager.getCarState(carId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CarStateResponse>() {
                    @Override
                    public void onCompleted() {
                        Log.i(TAG, "onCompleted: Retrieved car data");
                        if(getCar().getCarState().getLocked())
                            iv_home_lock.setImageDrawable(getResources().getDrawable(R.drawable.kl_lockclosed));
                        else
                            iv_home_lock.setImageDrawable(getResources().getDrawable(R.drawable.kl_lockopen));
                        pb_home_loadingIndicator.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: Failed to retrieve car data: " + e.getMessage());
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(CarStateResponse carStateResponse) {
                        CarState carState = carStateResponse.getCarState();
                        addRealmCarAttribute(carState);
                    }
                });
    }

    private void processCarBatteryData() {
        RealmCar car = getCar();
        tv_home_battery.setText("" + car.getBatteryState().getBatteryLevel() + " %");
        tv_home_distance.setText(String.format("%.0f km",car.getBatteryState().getBatteryRange()));
    }

    private void addRealmCarAttribute(Object attribute){
        RealmCar car = getCar();

        realm.beginTransaction();

        if(attribute.getClass() == ClimateState.class){
            ClimateState managedClimateState = realm.copyToRealm((ClimateState)attribute);
            car.setClimateState(managedClimateState);
        }
        else if(attribute.getClass() == BatteryState.class){
            BatteryState managedBatteryState = realm.copyToRealm((BatteryState)attribute);
            car.setBatteryState(managedBatteryState);
        }
        else if(attribute.getClass() == LocationState.class){
            LocationState managedLocationState = realm.copyToRealm((LocationState)attribute);
            car.setLocationState(managedLocationState);
        }
        else if(attribute.getClass() == CarState.class){
            CarState managedCarState = realm.copyToRealm((CarState) attribute);
            car.setCarState(managedCarState);
        }

        realm.commitTransaction();
    }

    @OnClick(R.id.iv_home_lock)
    public void onLockClicked(View view){
        pb_home_lockingIndicator.setVisibility(View.VISIBLE);
        iv_home_lock.setVisibility(View.INVISIBLE);
        if(getCar().getCarState().getLocked()){

            ApiManager
                    .unlockDoors(carId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<DefaultPostResponseWrapped>() {
                        @Override
                        public void onCompleted() {
                            Log.i(TAG, "onCompleted: Car unlocked by button");
                            realm.beginTransaction();
                            getCar().getCarState().setLocked(false);
                            realm.commitTransaction();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.i(TAG, "onError: Car failed to unlock by button");
                            e.printStackTrace();
                        }

                        @Override
                        public void onNext(DefaultPostResponseWrapped defaultPostResponseWrapped) {

                        }
                    });

            ApiManager
                    .remoteStart(carId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<DefaultPostResponseWrapped>() {
                        @Override
                        public void onCompleted() {
                            Log.i(TAG, "onCompleted: Car can be started");
                            pb_home_lockingIndicator.setVisibility(View.INVISIBLE);
                            iv_home_lock.setImageDrawable(getResources().getDrawable(R.drawable.kl_lockopen));
                            iv_home_lock.setVisibility(View.VISIBLE);
                            Toast.makeText(App.getContext(), "Open & ready to start!", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(TAG, "onError: failed to remote start " + e.getMessage());
                        }

                        @Override
                        public void onNext(DefaultPostResponseWrapped defaultPostResponseWrapped) {

                        }
                    });
        }
        else{
            ApiManager
                    .lockDoors(carId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<DefaultPostResponseWrapped>() {
                        @Override
                        public void onCompleted() {
                            Log.i(TAG, "onCompleted: Car locked by button");
                            realm.beginTransaction();
                            getCar().getCarState().setLocked(true);
                            realm.commitTransaction();
                            pb_home_lockingIndicator.setVisibility(View.INVISIBLE);
                            iv_home_lock.setImageDrawable(getResources().getDrawable(R.drawable.kl_lockclosed));
                            iv_home_lock.setVisibility(View.VISIBLE);
                            Toast.makeText(App.getContext(), "Safely Closed", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.i(TAG, "onError: Car failed to lock by button");
                            e.printStackTrace();
                        }

                        @Override
                        public void onNext(DefaultPostResponseWrapped defaultPostResponseWrapped) {

                        }
                    });

        }
    }


    @OnClick(R.id.iv_home_settings)
    public void showMenu(View view){
        bottomSheetMenuDialog.show();
        setupMenuDialog();
    }

}
