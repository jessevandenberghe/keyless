package be.appwise.keyless.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.ShowableListMenu;
import android.util.Log;

import com.orhanobut.hawk.Hawk;

import be.appwise.keyless.App;
import be.appwise.keyless.R;
import be.appwise.keyless.api.ApiManager;
import be.appwise.keyless.models.AccessToken;
import be.appwise.keyless.models.Car.AllCarsResponse;
import be.appwise.keyless.models.Realm.RealmCar;
import io.realm.Realm;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SplashActivity extends BaseActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();
    private static final int SPLASH_DISPLAY_LENGTH = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        checkActiveSession();
    }

    private void checkActiveSession() {
        Hawk.init(App.getContext()).build();
        AccessToken oAuthToken = Hawk.get("oAuthToken",null);
        boolean activeSession = oAuthToken != null;
        if(activeSession){
            login();
        }
        else{
            toLoginScreen();
        }
    }

    private void toLoginScreen() {
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent loginIntent = new Intent(SplashActivity.this,LoginActivity.class);
                SplashActivity.this.startActivity(loginIntent);
                SplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void login() {
        ApiManager
            .getCars()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Subscriber<AllCarsResponse>() {
                @Override
                public void onCompleted() {
                    new Handler().postDelayed(new Runnable(){
                        @Override
                        public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                            Intent loginIntent = new Intent(SplashActivity.this,HomeActivity.class);
                            SplashActivity.this.startActivity(loginIntent);
                            SplashActivity.this.finish();
                        }
                    }, SPLASH_DISPLAY_LENGTH - 1000);
                }

                @Override
                public void onError(Throwable e) {
                    Log.e(TAG, "onError: Error on getting car info: " + e.getMessage());
                    e.printStackTrace();
                }

                @Override
                public void onNext(final AllCarsResponse allCarsResponse) {
                    Realm realm = App.getCoreRealmInstance();
                    try {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                RealmCar car = new RealmCar(allCarsResponse.getCarList().get(0));
                                realm.copyToRealmOrUpdate(car);
                            }
                        });
                    } catch (Exception e){
                        e.printStackTrace();
                    } finally {
                        Log.i(TAG, "onNext: car retrieved");
                        realm.close();
                    }
                }
            });
    }
}
