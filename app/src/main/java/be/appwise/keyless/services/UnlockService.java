package be.appwise.keyless.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import be.appwise.keyless.App;
import be.appwise.keyless.api.ApiManager;
import be.appwise.keyless.models.Car.DefaultPostResponseWrapped;
import be.appwise.keyless.models.Realm.RealmCar;
import io.realm.Realm;
import layout.keyReplacerNotification;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by jessevandenberghe on 24/08/2017.
 */

public class UnlockService extends Service {

    private static final String TAG = UnlockService.class.getSimpleName();
    private static Realm realm;
    private static RealmCar realmcar;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ApiManager
                .unlockDoors(App.getCoreRealmInstance().where(RealmCar.class).findFirst().getCarInfo().getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<DefaultPostResponseWrapped>() {
                    @Override
                    public void onCompleted() {
                        //keyReplacerNotification.cancel(App.getContext());

                        realm = App.getCoreRealmInstance();
                        realmcar = realm.where(RealmCar.class).findFirst();
                        keyReplacerNotification.notify(App.getContext(),"" + realmcar.getCarInfo().getDisplayName() + ": Opened by user");
                        Log.i(TAG, "onCompleted: Car unlocked by notification");
                        realm.close();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i(TAG, "onError: Car failed to unlock by notification");
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(DefaultPostResponseWrapped defaultPostResponseWrapped) {

                    }
                });
        ApiManager
                .remoteStart(App.getCoreRealmInstance().where(RealmCar.class).findFirst().getCarInfo().getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<DefaultPostResponseWrapped>() {
                    @Override
                    public void onCompleted() {
                        Log.i(TAG, "onCompleted: Car can be started");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: failed to remote start " + e.getMessage());
                    }

                    @Override
                    public void onNext(DefaultPostResponseWrapped defaultPostResponseWrapped) {

                    }
                });
        return super.onStartCommand(intent, flags, startId);
    }
}
