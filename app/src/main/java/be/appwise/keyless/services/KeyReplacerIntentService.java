package be.appwise.keyless.services;

import android.Manifest;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.LocationRequest;

import be.appwise.keyless.App;
import be.appwise.keyless.api.ApiManager;
import be.appwise.keyless.models.Car.DefaultPostResponseWrapped;
import be.appwise.keyless.models.Car.LocationState;
import be.appwise.keyless.models.Car.LocationStateResponse;
import be.appwise.keyless.models.Realm.RealmCar;
import io.realm.Realm;
import layout.keyReplacerNotification;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by jessevandenberghe on 25/08/2017.
 */

public class KeyReplacerIntentService extends IntentService {

    private static final String TAG = KeyReplacerIntentService.class.getSimpleName();
    private static int counter = 0;
    private static Realm realm;
    private static RealmCar realmcar;
    private static LocationManager locationManager;
    private static Location userLocation;

    public KeyReplacerIntentService() {
        super(TAG);
        // Acquire a reference to the system Location Manager
        LocationRequest request = LocationRequest.create() //standard GMS LocationRequest
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setNumUpdates(Integer.MAX_VALUE)
                .setInterval(3000);

        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(App.getContext());
        if (ActivityCompat.checkSelfPermission(App.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(App.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        }
        locationProvider.getLastKnownLocation()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Location>() {
                    @Override
                    public void call(Location location) {
                        //Log.i(TAG, "call: location retrieved");
                        userLocation = location;
                    }
                });
        Subscription subscription = locationProvider.getUpdatedLocation(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Location>() {
                    @Override
                    public void call(Location location) {
                        userLocation = location;
                        //Log.i(TAG, "call: location updated");
                    }
                });
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.i(TAG, "onHandleIntent: running: " + App.getKeyReplacerIntentRunning() + " InGeofence: " + App.getInGeoFence());
        if(App.getKeyReplacerIntentRunning()){
            if(App.getInGeoFence()) {
                counter++;
                realm = App.getCoreRealmInstance();
                realmcar = realm.where(RealmCar.class).findFirst();
                LocationState locationState = realmcar.getLocationState();
                Location carLocation = new Location("car");
                if (userLocation != null && locationState != null) {
                    carLocation.setLatitude(locationState.getLatitude());
                    carLocation.setLongitude(locationState.getLongitude());
                    Log.i(TAG, "run: distance to car: " + carLocation.distanceTo(userLocation));
                    if (carLocation.distanceTo(userLocation) < 20) {
                        unlockCar();
                    } else {
                        lockCar();
                    }
                }

                if (counter == 6) {
                    counter = 0;
                    updateCarLocation();
                }
                realm.close();
            }

            scheduleNextUpdate();
        }
        else
            stopSelf();
    }

    private void scheduleNextUpdate() {
        Intent intent = new Intent(this, this.getClass());
        PendingIntent pendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        long currentTimeMillis = System.currentTimeMillis();
        long nextUpdateTimeMillis = currentTimeMillis + 3 * DateUtils.SECOND_IN_MILLIS;
        Time nextUpdateTime = new Time();
        nextUpdateTime.set(nextUpdateTimeMillis);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC, nextUpdateTimeMillis, pendingIntent);
    }

    private void lockCar() {
        if(!realmcar.getCarState().getLocked()){
            ApiManager
                    .lockDoors(App.getCoreRealmInstance().where(RealmCar.class).findFirst().getCarInfo().getId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<DefaultPostResponseWrapped>() {
                        @Override
                        public void onCompleted() {
                            realm = App.getCoreRealmInstance();
                            realmcar = realm.where(RealmCar.class).findFirst();
                            Log.i(TAG, "lockCar: Car locked by location");
                            keyReplacerNotification.notify(App.getContext(),"To far from " + realmcar.getCarInfo().getDisplayName() + ": Closed");
                            Toast.makeText(App.getContext(),"Car Locked by location", Toast.LENGTH_SHORT).show();
                            realm.beginTransaction();
                            realmcar.getCarState().setLocked(true);
                            realm.commitTransaction();
                            realm.close();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.i(TAG, "onError: Car failed to lock by location");
                            e.printStackTrace();
                        }

                        @Override
                        public void onNext(DefaultPostResponseWrapped defaultPostResponseWrapped) {

                        }
                    });
        }
    }

    private void unlockCar() {
        if(realmcar.getCarState().getLocked()){
            ApiManager
                    .unlockDoors(App.getCoreRealmInstance().where(RealmCar.class).findFirst().getCarInfo().getId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<DefaultPostResponseWrapped>() {
                        @Override
                        public void onCompleted() {
                            Log.i(TAG, "lockCar: Car unlocked by location");
                            realm = App.getCoreRealmInstance();
                            realmcar = realm.where(RealmCar.class).findFirst();
                            keyReplacerNotification.notify(App.getContext(),"Close to " + realmcar.getCarInfo().getDisplayName() + ": Open");
                            Toast.makeText(App.getContext(),"Car Unlocked by location", Toast.LENGTH_SHORT).show();
                            realm.beginTransaction();
                            realmcar.getCarState().setLocked(false);
                            realm.commitTransaction();
                            realm.close();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.i(TAG, "onError: Car failed to lock by location");
                            e.printStackTrace();
                        }

                        @Override
                        public void onNext(DefaultPostResponseWrapped defaultPostResponseWrapped) {

                        }
                    });
            ApiManager
                    .remoteStart(App.getCoreRealmInstance().where(RealmCar.class).findFirst().getCarInfo().getId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<DefaultPostResponseWrapped>() {
                        @Override
                        public void onCompleted() {
                            Log.i(TAG, "onCompleted: Car can be started");
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(TAG, "onError: failed to remote start " + e.getMessage());
                        }

                        @Override
                        public void onNext(DefaultPostResponseWrapped defaultPostResponseWrapped) {

                        }
                    });

        }

    }

    private void updateCarLocation(){
        ApiManager.getLocationState(realmcar.getCarInfo().getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LocationStateResponse>() {
                    @Override
                    public void onCompleted() {
                        Log.i(TAG, "onCompleted: Retrieved location data");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: Failed to retrieve location data: " + e.getMessage());
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(LocationStateResponse locationStateResponse) {
                        LocationState locationState = locationStateResponse.getLocationState();
                        realm = App.getCoreRealmInstance();
                        realmcar = realm.where(RealmCar.class).findFirst();
                        realm.beginTransaction();
                        LocationState managedLocationState = realm.copyToRealm(locationStateResponse.getLocationState());
                        realmcar.setLocationState(managedLocationState);
                        realm.commitTransaction();
                        realm.close();

                    }
                });
    }
}
