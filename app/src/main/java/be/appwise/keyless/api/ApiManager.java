package be.appwise.keyless.api;

import android.util.Log;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.orhanobut.hawk.Hawk;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;


import be.appwise.keyless.App;
import be.appwise.keyless.models.AccessToken;
import be.appwise.keyless.models.Car.AllCarsResponse;
import be.appwise.keyless.models.Car.BatteryStateResponse;
import be.appwise.keyless.models.Car.CarStateResponse;
import be.appwise.keyless.models.Car.ClimateStateResponse;
import be.appwise.keyless.models.Car.DefaultPostResponseWrapped;
import be.appwise.keyless.models.Car.LocationStateResponse;
import be.appwise.keyless.models.Realm.RealmString;
import io.realm.RealmList;
import io.realm.RealmObject;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Emitter;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * Created by 11501253 on 16/08/2017.
 */

public class ApiManager{
    private static final String TAG = ApiManager.class.getSimpleName();

    //private static final String ENDPOINT = "https://private-anon-9d0048ea8f-timdorr.apiary-mock.com/";
    private static final String ENDPOINT = "https://owner-api.teslamotors.com/";
    //private static final String CLIENTID = "abc";
    public static final String CLIENTID = "81527cff06843c8634fdc09e8ac0abefb46ac849f38fe1e431c2ef2106796384";
    //private static final String CLIENTSECRET = "123";
    public static final String CLIENTSECRET = "c7257eb71a564034f9419ee651c7d0e5f7aa6bfbd18bafb5c5c033b093bb2fa3";
    public static final OkHttpClient CLIENT  = getClient();

    private static final Type token = new TypeToken<RealmList<RealmString>>() {
    }.getType();

    private static final Gson gson = new GsonBuilder()
            .setExclusionStrategies(new ExclusionStrategy() {
                @Override
                public boolean shouldSkipField(FieldAttributes f) {
                    return f.getDeclaringClass().equals(RealmObject.class);
                }

                @Override
                public boolean shouldSkipClass(Class<?> clazz) {
                    return false;
                }
            })
            .registerTypeAdapter(token, new TypeAdapter<RealmList<RealmString>>() {

                @Override
                public void write(JsonWriter out, RealmList<RealmString> value) throws IOException {
                    // Ignore
                    out.beginArray();

                    for (int i = 0; i < value.size(); i++) {
                        out.value(value.get(i).getName());
                    }

                    out.endArray();
                }

                @Override
                public RealmList<RealmString> read(JsonReader in) throws IOException {
                    RealmList<RealmString> list = new RealmList<RealmString>();
                    if (in.peek() == JsonToken.NULL) {
                        in.nextNull();
                        return list;
                    } else {
                        in.beginArray();
                        while (in.hasNext()) {
                            list.add(new RealmString(in.nextString()));
                        }
                        in.endArray();
                        return list;
                    }
                }
            })
            .setLenient()
            .create();

    private static final Retrofit retrofit = new Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(ENDPOINT)
            .client(CLIENT)
            .build();

    public static final ApiManagerService apiManager = retrofit.create(ApiManagerService.class);


    private static OkHttpClient getClient() {
        //zorgt voor logging van de http requests
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient()
                .newBuilder()
                .addInterceptor(interceptor)
                .addInterceptor(new AuthenticateInterceptor())
                .connectTimeout(45, TimeUnit.SECONDS)
                .readTimeout(45, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    public static String getENDPOINT() {
        return ENDPOINT;
    }

    public static String getCLIENTID() {
        return CLIENTID;
    }

    public static String getCLIENTSECRET() {
        return CLIENTSECRET;
    }

    public static OkHttpClient getCLIENT() {
        return CLIENT;
    }

    public static Observable<AccessToken> getAccessToken(final String email, final String password){
        return Observable.create(new Observable.OnSubscribe<AccessToken>(){

            @Override
            public void call(Subscriber<? super AccessToken> subscriber) {
                if(!subscriber.isUnsubscribed()){
                    try {
                        Response<AccessToken> response = apiManager.getAccessToken("password",CLIENTID,CLIENTSECRET, email, password).execute();
                        if(response.isSuccessful()){
                            subscriber.onNext(response.body());
                            subscriber.onCompleted();
                        }
                        else{
                            Log.e(TAG, "error on getting access token: ");
                            handleError(subscriber,null,null,response);
                        }
                    }
                    catch (Exception e){
                        handleError(subscriber,null,e,null);
                    }
                }
            }
        }).subscribeOn(Schedulers.io());

    }

    public static Observable<AllCarsResponse> getCars(){
        return Observable.create(new Observable.OnSubscribe<AllCarsResponse>(){
            @Override
            public void call(Subscriber<? super AllCarsResponse> subscriber) {

                if (!subscriber.isUnsubscribed()) {
                    try {
                        Response<AllCarsResponse> response = apiManager.getCars(/*"Bearer " + accessToken*/).execute();
                        if (response.isSuccessful()) {
                            subscriber.onNext(response.body());
                            subscriber.onCompleted();
                        } else {
                            Log.d(TAG, "Error on getting cars");
                            handleError(subscriber,null, null, response);
                        }
                    } catch (Exception ex) {
                        handleError(subscriber,null, ex, null);
                    }
                }
            }
        }).subscribeOn(Schedulers.io());
    }

    public static Observable<BatteryStateResponse> getBatteryState(final Long carId){
        return Observable.create(new Observable.OnSubscribe<BatteryStateResponse>() {
            @Override
            public void call(Subscriber<? super BatteryStateResponse> subscriber) {
                if(!subscriber.isUnsubscribed()){
                    try {
                        Response<BatteryStateResponse> response = apiManager.getBatteryState(carId/*,"Bearer " + accessToken*/).execute();
                        //Response<BatteryStateResponse> response = apiManager.getBatteryState(1,accessToken).execute();
                        if(response.isSuccessful()){
                            subscriber.onNext(response.body());
                            subscriber.onCompleted();
                        }
                        else{
                            handleError(subscriber,null,null,response);
                        }
                    }
                    catch (Exception ex){
                        handleError(subscriber,null,ex,null);
                    }
                }
            }
        });
    }

    public static Observable<ClimateStateResponse> getClimateState(final Long carId){
        return Observable.create(new Observable.OnSubscribe<ClimateStateResponse>() {
            @Override
            public void call(Subscriber<? super ClimateStateResponse> subscriber) {
                if(!subscriber.isUnsubscribed()){
                    try {
                        Response<ClimateStateResponse> response = apiManager.getClimateState(carId/*, "Bearer " + accessToken¨*/).execute();
                        if(response.isSuccessful()){
                            subscriber.onNext(response.body());
                            subscriber.onCompleted();
                        }
                        else{
                            handleError(subscriber,null,null,response);
                        }
                    }
                    catch (Exception ex){
                        handleError(subscriber,null,ex,null);
                    }
                }
            }
        });
    }

    public static Observable<LocationStateResponse> getLocationState(final Long carId){
        return Observable.create(new Observable.OnSubscribe<LocationStateResponse>() {
            @Override
            public void call(Subscriber<? super LocationStateResponse> subscriber) {
                if(!subscriber.isUnsubscribed()){
                    try {
                        Response<LocationStateResponse> response = apiManager.getLocationState(carId/*, "Bearer " + accessToken¨*/).execute();
                        if(response.isSuccessful()){
                            subscriber.onNext(response.body());
                            subscriber.onCompleted();
                        }
                        else{
                            handleError(subscriber,null,null,response);
                        }
                    }
                    catch (Exception ex){
                        handleError(subscriber,null,ex,null);
                    }
                }
            }
        });
}

    public static Observable<CarStateResponse> getCarState(final Long carId){
        return Observable.create(new Observable.OnSubscribe<CarStateResponse>() {
            @Override
            public void call(Subscriber<? super CarStateResponse> subscriber) {
                if(!subscriber.isUnsubscribed()){
                    try {
                        Response<CarStateResponse> response = apiManager.getCarState(carId).execute();
                        if(response.isSuccessful()){
                            subscriber.onNext(response.body());
                            subscriber.onCompleted();
                        }
                        else{
                            handleError(subscriber,null,null,response);
                        }
                    }
                    catch (Exception ex){
                        handleError(subscriber,null,ex,null);
                    }
                }
            }
        });
    }

    public static Observable<DefaultPostResponseWrapped> setClimateState(final Long carId, final double driverDegC, final double passDegC){
        return Observable.create(new Observable.OnSubscribe<DefaultPostResponseWrapped>() {
            @Override
            public void call(Subscriber<? super DefaultPostResponseWrapped> subscriber) {
                if(!subscriber.isUnsubscribed()){
                    try {
                        Response<DefaultPostResponseWrapped> response = apiManager.setClimateState(carId, driverDegC, passDegC/*, "Bearer " + accessToken*/).execute();
                        if(response.isSuccessful()){
                            subscriber.onNext(response.body());
                            subscriber.onCompleted();
                        }
                        else{
                            handleError(subscriber,null,null,response);
                        }
                    }
                    catch (Exception ex){
                        handleError(subscriber,null,ex,null);
                    }
                }
            }
        });
    }

    public static Observable<DefaultPostResponseWrapped> honkHorn(final Long carId){
        return Observable.create(new Observable.OnSubscribe<DefaultPostResponseWrapped>() {
            @Override
            public void call(Subscriber<? super DefaultPostResponseWrapped> subscriber) {
                if(!subscriber.isUnsubscribed()){
                    try {
                        Response<DefaultPostResponseWrapped> response = apiManager.honkHorn(carId/*, "Bearer " + accessToken*/).execute();
                        if(response.isSuccessful()){
                            subscriber.onNext(response.body());
                            subscriber.onCompleted();
                        }
                        else{
                            handleError(subscriber,null,null,response);
                        }
                    }
                    catch (Exception ex){
                        handleError(subscriber,null,ex,null);
                    }
                }
            }
        });
    }

    public static Observable<DefaultPostResponseWrapped> lockDoors(final Long carId){
        return Observable.create(new Observable.OnSubscribe<DefaultPostResponseWrapped>() {
            @Override
            public void call(Subscriber<? super DefaultPostResponseWrapped> subscriber) {
                try{
                    Response<DefaultPostResponseWrapped> response = apiManager.lockDoors(carId).execute();
                    if(response.isSuccessful()){
                        subscriber.onNext(response.body());
                        subscriber.onCompleted();
                    }
                    else{
                        handleError(subscriber,null,null,response);
                    }
                }
                catch (Exception ex){
                    handleError(subscriber,null,ex,null);
                }
            }
        });
    }

    public static Observable<DefaultPostResponseWrapped> unlockDoors(final Long carId){
        return Observable.create(new Observable.OnSubscribe<DefaultPostResponseWrapped>() {
            @Override
            public void call(Subscriber<? super DefaultPostResponseWrapped> subscriber) {
                try{
                    Response<DefaultPostResponseWrapped> response = apiManager.unlockDoors(carId).execute();
                    if(response.isSuccessful()){
                        subscriber.onNext(response.body());
                        subscriber.onCompleted();
                    }
                    else{
                        handleError(subscriber,null,null,response);
                    }
                }
                catch (Exception ex){
                    handleError(subscriber,null,ex,null);
                }
            }
        });
    }

    public static Observable<DefaultPostResponseWrapped> flashLights(final Long carId){
        return Observable.create(new Observable.OnSubscribe<DefaultPostResponseWrapped>() {
            @Override
            public void call(Subscriber<? super DefaultPostResponseWrapped> subscriber) {
                try{
                    Response<DefaultPostResponseWrapped> response = apiManager.flashLights(carId).execute();
                    if(response.isSuccessful()){
                        subscriber.onNext(response.body());
                        subscriber.onCompleted();
                    }
                    else{
                        handleError(subscriber,null,null,response);
                    }
                }
                catch (Exception ex){
                    handleError(subscriber,null,ex,null);
                }
            }
        });
    }

    public static Observable<DefaultPostResponseWrapped> openChargePort(final Long carId){
        return Observable.create(new Observable.OnSubscribe<DefaultPostResponseWrapped>() {
            @Override
            public void call(Subscriber<? super DefaultPostResponseWrapped> subscriber) {
                try{
                    Response<DefaultPostResponseWrapped> response = apiManager.openChargePort(carId).execute();
                    if(response.isSuccessful()){
                        subscriber.onNext(response.body());
                        subscriber.onCompleted();
                    }
                    else {
                        handleError(subscriber,null,null,response);
                    }
                }
                catch (Exception ex){
                    handleError(subscriber,null,ex,null);
                }
            }
        });
    }

    public static Observable<DefaultPostResponseWrapped> startHvac(final Long carId){
        return Observable.create(new Observable.OnSubscribe<DefaultPostResponseWrapped>() {
            @Override
            public void call(Subscriber<? super DefaultPostResponseWrapped> subscriber) {
                try{
                    Response<DefaultPostResponseWrapped> response = apiManager.startHvac(carId).execute();
                    if(response.isSuccessful()){
                        subscriber.onNext(response.body());
                        subscriber.onCompleted();
                    }
                    else {
                        handleError(subscriber,null,null,response);
                    }
                }
                catch (Exception ex){
                    handleError(subscriber,null,ex,null);
                }
            }
        });
    }

    public static Observable<DefaultPostResponseWrapped> stopHvac(final Long carId){
        return Observable.create(new Observable.OnSubscribe<DefaultPostResponseWrapped>() {
            @Override
            public void call(Subscriber<? super DefaultPostResponseWrapped> subscriber) {
                try{
                    Response<DefaultPostResponseWrapped> response = apiManager.stopHvac(carId).execute();
                    if(response.isSuccessful()){
                        subscriber.onNext(response.body());
                        subscriber.onCompleted();
                    }
                    else {
                        handleError(subscriber,null,null,response);
                    }
                }
                catch (Exception ex){
                    handleError(subscriber,null,ex,null);
                }
            }
        });
    }

    public static Observable<DefaultPostResponseWrapped> remoteStart(final Long carId){
        Hawk.init(App.getContext()).build();
        return Observable.create(new Observable.OnSubscribe<DefaultPostResponseWrapped>() {
            @Override
            public void call(Subscriber<? super DefaultPostResponseWrapped> subscriber) {
                try{
                    Response<DefaultPostResponseWrapped> response = apiManager.remoteStart(carId, (String) Hawk.get("password")).execute();
                    if(response.isSuccessful()){
                        subscriber.onNext(response.body());
                        subscriber.onCompleted();
                    }
                    else {
                        handleError(subscriber,null,null,response);
                    }
                }
                catch (Exception ex){
                    handleError(subscriber,null,ex,null);
                }
            }
        });
    }

    public static void handleError(Subscriber subscriber, Emitter emitter, Exception ex, Response response){
        ex.printStackTrace();
        if (subscriber != null) {
            subscriber.onCompleted();
        }
        if (emitter != null){
            emitter.onError(new Exception("Whoops, something went wrong"));
        }
    }

}
