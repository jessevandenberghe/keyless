package be.appwise.keyless.api;

import android.os.Handler;
import android.util.Log;

import com.orhanobut.hawk.Hawk;

import java.io.IOException;

import be.appwise.keyless.App;
import be.appwise.keyless.events.LogoutEvent;
import be.appwise.keyless.models.AccessToken;
import okhttp3.Interceptor;
import okhttp3.Request;
import retrofit2.Response;


public class AuthenticateInterceptor implements Interceptor {
    public final String TAG =  AuthenticateInterceptor.class.getSimpleName();
    public AuthenticateInterceptor() {
    }

    @Override
    public okhttp3.Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Hawk.init(App.getContext()).build();
        AccessToken oauthToken = Hawk.get("oAuthToken",null);

        String responseRequestUrl = request.url().toString();
        if (responseRequestUrl.contains("oauth") || oauthToken == null) {
            okhttp3.Response response = chain.proceed(request);
            if (!response.isSuccessful()) {
                logout();
                return response;
            } else {
                return response;
            }
        }

        Request.Builder requestBuilder = request.newBuilder();
        try {
            requestBuilder.addHeader("Authorization", "Bearer " + oauthToken.getAccessToken());
        } catch (Exception e){
            e.printStackTrace();
        }
        Request newRequest = requestBuilder.build();
        okhttp3.Response response = chain.proceed(newRequest);

        if (response.code() == 401) {
            //Unauthorised => try refresh
            String newAccessToken = "";

            synchronized (ApiManager.CLIENT) { //perform all 401 in sync blocks, to avoid multiply token updates

                try {
                    Log.i(TAG, "Refreshing token ");
                    Response refreshResponse = ApiManager.apiManager.refreshAccessToken("refresh_token", ApiManager.CLIENTID, ApiManager.CLIENTSECRET, oauthToken.getRefreshToken()).execute();

                    if (refreshResponse.isSuccessful()) {
                        AccessToken newOauthToken = (AccessToken) refreshResponse.body();
                        newOauthToken.setRefreshToken(oauthToken.getRefreshToken());

                        Hawk.put("oAuthToken",newOauthToken);

                        Log.i(TAG, "TokenAuthenticator refreshtoken response " + newOauthToken.getAccessToken());

                        newAccessToken = "Bearer " + newOauthToken.getAccessToken();
                    } else if (refreshResponse.code() / 100 == 4) {
                        logout();
                    }
                } catch (Exception e) {
                    Log.d(TAG, "Exception in refreshToken ");
                    e.printStackTrace();
                }
            }

            if (newAccessToken.isEmpty()
                    || response.request().header("Authorization").equalsIgnoreCase(newAccessToken)) {
                logout();
                return response;
            }

            Request.Builder authRequestBuilder = chain.request().newBuilder();
            authRequestBuilder.addHeader("Authorization", newAccessToken);
            response = chain.proceed(authRequestBuilder.build());
            if (response.code() == 401) {
                logout();
                return response;
            } else {
                return response;
            }
        } else {
            return response;
        }
    }

    private int responseCount(okhttp3.Response response) {
        int result = 1;
        while ((response = response.priorResponse()) != null) {
            result++;
        }
        return result;
    }

    void logout() {

        Handler mainHandler = new Handler(App.getContext().getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                App.getBus().post(new LogoutEvent());
            } // This is your code
        };
        mainHandler.post(myRunnable);
    }
}