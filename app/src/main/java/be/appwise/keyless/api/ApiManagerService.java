package be.appwise.keyless.api;


import be.appwise.keyless.models.AccessToken;
import be.appwise.keyless.models.Car.AllCarsResponse;
import be.appwise.keyless.models.Car.BatteryStateResponse;
import be.appwise.keyless.models.Car.CarStateResponse;
import be.appwise.keyless.models.Car.ClimateStateResponse;
import be.appwise.keyless.models.Car.DefaultPostResponseWrapped;
import be.appwise.keyless.models.Car.LocationStateResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by 11501253 on 16/08/2017.
 */

public interface ApiManagerService {
    //oAuth get access token
    @POST("oauth/token")
    @FormUrlEncoded
    Call<AccessToken> getAccessToken(@Field("grant_type") String grantType, @Field("client_id") String clientId, @Field("client_secret") String clientSecret, @Field("email") String email, @Field("password") String password);
    @POST("oauth/token")
    @FormUrlEncoded
    Call<AccessToken> refreshAccessToken(@Field("grant_type") String grantType, @Field("client_id") String clientId, @Field("client_secret") String clientSecret, @Field("refresh_token") String refreshToken);
    //get All vehicles from user
    @GET("api/1/vehicles")
    Call<AllCarsResponse> getCars(/*@Header("Authorization") String accessToken*/);
    //get charging state of selected vehicle
    @GET("api/1/vehicles/{vehicle_id}/data_request/charge_state")
    Call<BatteryStateResponse> getBatteryState(@Path("vehicle_id") Long vehicleId);
    //get climate state of selected vehicle
    @GET("api/1/vehicles/{vehicle_id}/data_request/climate_state")
    Call<ClimateStateResponse> getClimateState(@Path("vehicle_id") Long vehicleId);
    //get location of selected vehicle
    @GET("api/1/vehicles/{vehicle_id}/data_request/drive_state")
    Call<LocationStateResponse> getLocationState(@Path("vehicle_id") Long vehicleId);
    //get the car state of selected vehicle
    @GET("api/1/vehicles/{vehicle_id}/data_request/vehicle_state")
    Call<CarStateResponse> getCarState(@Path("vehicle_id") Long vehicleId);
    //set climate state of selected vehicle
    @POST("/api/1/vehicles/{vehicle_id}/command/set_temps")
    Call<DefaultPostResponseWrapped> setClimateState(@Path("vehicle_id") Long vehicleId, @Query("driver_degC") double driverDegC, @Query("pass_degC") double passDegC);
    //honks horn of selected vehicle
    @POST("api/1/vehicles/{vehicle_id}/command/honk_horn")
    Call<DefaultPostResponseWrapped> honkHorn(@Path("vehicle_id") Long vehicleId);
    //locks door of selected vehicle
    @POST("api/1/vehicles/{vehicle_id}/command/door_lock")
    Call<DefaultPostResponseWrapped> lockDoors(@Path("vehicle_id") Long vehicleId);
    //unlocks door of selected vehicle
    @POST("api/1/vehicles/{vehicle_id}/command/door_unlock")
    Call<DefaultPostResponseWrapped> unlockDoors(@Path("vehicle_id") Long vehicleId);
    //opens trunk/frunk of selected vehicle
    /*@POST("api/1/vehicles/{vehicle_id}/command/trunk_open")  --> inoperable
    Call<DefaultPostResponseWrapped> openTrunk(@Path("vehicle_id") Long vehicleId, @Query("which_trunk") String whichTrunk);*/
    //flashes lights of selected vehicle
    @POST("api/1/vehicles/{vehicle_id}/command/flash_lights")
    Call<DefaultPostResponseWrapped> flashLights(@Path("vehicle_id") Long vehicleId);
    //opens charge port of selected vehicle
    @POST("api/1/vehicles/{vehicle_id}/command/charge_port_door_open")
    Call<DefaultPostResponseWrapped> openChargePort(@Path("vehicle_id") Long vehicleId);
    //starts HVAC system
    @POST("api/1/vehicles/{vehicle_id}/command/auto_conditioning_start")
    Call<DefaultPostResponseWrapped> startHvac(@Path("vehicle_id") Long vehicleId);
    //stops HVAC system
    @POST("api/1/vehicles/{vehicle_id}/command/auto_conditioning_start")
    Call<DefaultPostResponseWrapped> stopHvac(@Path("vehicle_id") Long vehicleId);
    //Remote start car
    @POST("api/1/vehicles/{vehicle_id}/command/remote_start_drive")
    Call<DefaultPostResponseWrapped> remoteStart(@Path("vehicle_id") Long vehicleId, @Query("password") String password);


}
